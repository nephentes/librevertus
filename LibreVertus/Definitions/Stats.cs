﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreVertus.Definitions
{
    public class Stats
    {

        public DateTime? WhenStarted { get; set; }

        public int RequestCount { get; set; }

        public decimal TotalWorkingTimeSec { get; set; }

        public decimal MeanWorkingTime
        {
            get
            {
                return TotalWorkingTimeSec / RequestCount;
            }
        }

        public Stats()
        {
            WhenStarted = DateTime.Now;
            RequestCount = 0;
        }

    }
}
