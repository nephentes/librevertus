﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LibreVertus.Definitions
{
    [DataContract]
    public class GenericResponse<T>
    {

        [DataMember]
        public decimal ProcessingTimeMS { get; set; }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public T Data { get; set; }

        [DataMember]
        public ResultCodes ResultCode { get; set; }

        public GenericResponse()
        {

        }

        public GenericResponse(T data) : this()
        {
            Data = data;
        }

    }

    [DataContract]
    public enum ResultCodes
    {
        [EnumMember]
        OK = 1,
        [EnumMember]
        WrongHash = 2,
        [EnumMember]
        Exception = 3
    }
}
