﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LibreVertus.Definitions
{

    [DataContract]
    public class ConvertRequest
    {

        [DataMember]
        public string ClientHash { get; set; }

        [DataMember]
        public FileInfo File { get; set; }

        [DataMember]
        public FileType ConvertTo { get; set; }

        [DataMember]
        public int? MaxWidth { get; set; }

        [DataMember]
        public int? MaxHeight { get; set; }

        public ConvertRequest()
        {

        }

    }

}
