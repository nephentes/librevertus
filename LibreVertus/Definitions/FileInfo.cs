﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LibreVertus.Definitions
{
    [DataContract]
    public class FileInfo
    {

        [DataMember]
        public byte[] Data { get; set; }

        [DataMember]
        public FileType Extension { get; set; }

    }

    [DataContract]
    public enum FileType
    {
        [EnumMember]
        DOCX = 1,
        [EnumMember]
        PDF = 2,
        [EnumMember]
        PNG = 3
    }
}
