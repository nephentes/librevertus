﻿using LibreVertus.Definitions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LibreVertus
{
    public static class LibreOfficeService
    {

        private static MD5 md5Hash = MD5.Create();

        private static string GetMd5Hash(byte[] content)
        {
            byte[] data = md5Hash.ComputeHash(content);
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString().Replace("-", "");
        }

        public static byte[] ConvertTo(ConvertRequest request)
        {
            byte[] retVal = null;

            var tempPath = Path.GetTempPath();
            var md5 = GetMd5Hash(request.File.Data);
            var ext = request.File.Extension.ToString().ToLower();
            var extDest = request.ConvertTo.ToString().ToLower();

            var sourceFile = tempPath + md5 + "." + ext;
            var destFile = tempPath + md5 + "." + extDest;

            
            if (Properties.Settings.Default.UseCache && File.Exists(destFile))
            {
                retVal = File.ReadAllBytes(destFile);
            }
            else
            {
                try
                {
                    File.WriteAllBytes(sourceFile, request.File.Data);

                    string commandArgs = string.Format(Properties.Settings.Default.ConvertCommand, request.ConvertTo.ToString().ToLower(), sourceFile);

                    ProcessStartInfo startInfo = new ProcessStartInfo(Properties.Settings.Default.SOfficePath, commandArgs);
                    startInfo.UseShellExecute = true;
                    startInfo.WorkingDirectory = tempPath;

                    var soffice = Process.Start(startInfo);
                    soffice.WaitForExit();

                    var fileContent = File.ReadAllBytes(destFile);

                    if (request.ConvertTo == FileType.PNG)
                    {
                        if (request.MaxHeight.HasValue || request.MaxWidth.HasValue)
                        {
                            MemoryStream ms = new MemoryStream(fileContent);
                            Image image = Image.FromStream(ms);
                            int newWidth = 100;
                            int newHeight = 100;

                            if (request.MaxWidth.HasValue && (image.Width > request.MaxWidth.Value))
                            {
                                newWidth = request.MaxWidth.Value;
                                newHeight = image.Height * request.MaxWidth.Value / image.Width;
                            }

                            if (request.MaxHeight.HasValue && (image.Height > request.MaxHeight.Value))
                            {
                                newWidth = image.Width * request.MaxHeight.Value / image.Height;
                                newHeight = request.MaxHeight.Value;
                            }

                            Image resizedImage = image.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);
                            MemoryStream msOut = new MemoryStream();
                            resizedImage.Save(msOut, System.Drawing.Imaging.ImageFormat.Png);
                            retVal = msOut.ToArray();
                        }
                        else
                        {
                            retVal = fileContent;
                        }
                    }
                    else
                    {
                        retVal = fileContent;
                    }

                }
                finally
                {
                    File.Delete(sourceFile);
                    if (!Properties.Settings.Default.UseCache)
                    {
                        File.Delete(destFile);
                    }
                }
            }

            return retVal;
        }

    }
}
