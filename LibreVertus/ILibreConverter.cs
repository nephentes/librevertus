﻿using LibreVertus.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LibreVertus
{
    
    [ServiceContract]
    public interface ILibreConverter
    {

        [OperationContract]
        GenericResponse<FileInfo> Convert(ConvertRequest request);

        [OperationContract]
        GenericResponse<Stats> GetStatus(string clientHash);

    }

}
