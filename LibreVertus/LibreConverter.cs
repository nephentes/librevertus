﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using LibreVertus.Definitions;

namespace LibreVertus
{
    
    public class LibreConverter : ILibreConverter
    {

        private static Stats Stat = new Stats();

        public GenericResponse<FileInfo> Convert(ConvertRequest request)
        {
            GenericResponse<FileInfo> retVal = new GenericResponse<FileInfo>();

            if (Properties.Settings.Default.ClientHash.Contains(request.ClientHash))
            {
                lock(Stat)
                {
                    Stat.RequestCount += 1;
                }

                DateTime whenStarted = DateTime.Now;

                try
                {
                    retVal.Data = new FileInfo();
                    retVal.Data.Extension = request.ConvertTo;
                    retVal.Data.Data = LibreOfficeService.ConvertTo(request);
                }
                catch (Exception ex)
                {
                    retVal.Success = false;
                    retVal.ResultCode = ResultCodes.WrongHash;
                    retVal.Message = ex.Message;
                    return retVal;
                }

                DateTime whenEnded = DateTime.Now;
                var howLong = whenEnded.Subtract(whenStarted);

                lock (Stat)
                {
                    Stat.TotalWorkingTimeSec += System.Convert.ToDecimal(howLong.TotalSeconds);
                }

                retVal.ProcessingTimeMS = System.Convert.ToDecimal(howLong.TotalMilliseconds);
                retVal.Success = true;
                retVal.ResultCode = ResultCodes.OK;
            }
            else
            {
                retVal.Success = false;
                retVal.ResultCode = ResultCodes.WrongHash;
            }

            return retVal;
        }

        public GenericResponse<Stats> GetStatus(string clientHash)
        {
            GenericResponse<Stats> retVal = new GenericResponse<Stats>();

            if (Properties.Settings.Default.ClientHash.Contains(clientHash))
            {
                lock (Stat)
                {
                    retVal.Data = Stat;
                    retVal.Success = true;
                    retVal.ResultCode = ResultCodes.OK;
                }
            }
            else
            {
                retVal.Success = false;
                retVal.ResultCode = ResultCodes.WrongHash;
            }

            return retVal;
        }
    }
}
