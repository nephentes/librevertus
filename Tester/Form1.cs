﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tester.ServiceReference1;

namespace Tester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog()== DialogResult.OK)
            {
              
                using (var cc = new LibreConverterClient())
                {
                    ConvertRequest request = new ConvertRequest();
                    request.ClientHash = "hash1";
                    request.File = new ServiceReference1.FileInfo();
                    request.File.Data = File.ReadAllBytes(ofd.FileName);
                    request.File.Extension = FileType.DOCX;
                    request.ConvertTo = FileType.PNG;
                    request.MaxHeight = 256;

                    var response = cc.Convert(request);

                    if(response.Success)
                    {
                        using (var ms = new MemoryStream(response.Data.Data))
                        {
                            pictureBox1.Image = Image.FromStream(ms);
                        }
                    } else
                    {
                        MessageBox.Show(response.Message);
                    }
                }
            }
        }
    }
}

