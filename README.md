### LibreVertus ###

WCF service for converting document files.

Input format: **DOCX**.

Output format: **PDF**, **PNG**.

#Example#

```
#!C#

using (var cc = new LibreConverterClient())
{
    ConvertRequest request = new ConvertRequest();
    request.ClientHash = "hash1";
    request.File = new ServiceReference1.FileInfo();
    request.File.Data = File.ReadAllBytes(ofd.FileName);
    request.File.Extension = FileType.DOCX;
    request.ConvertTo = FileType.PNG;
    request.MaxHeight = 256;

    var response = cc.Convert(request);

    if(response.Success)
    {
        using (var ms = new MemoryStream(response.Data.Data))
    {
        pictureBox1.Image = Image.FromStream(ms);
    }
    } else
    {
        MessageBox.Show(response.Message);
    }
}
```